---
# The main domain we will use for everything
domain: eestec.pp.ua

# The mail contact in case there are problems
admin_email: it-leader@eestec.net

# Differentiate between production/staging/development
server_type: __Development__
server_name: EESTEC_Development
repository_branch: dev
hostname: eestec-dev

# Show debug trace on browser
be_debug_browser: true

# Sometimes you also need the domain of another service, which is why we define
# them for all services here and not in group_vars.
ec_domain: "eestechchallenge.{{ domain }}"
ec_second_domain: "ec.{{ domain }}"
ssa_domain: "ssa.{{ domain }}"
ssa_wp_domain: "ssa-wp.{{ domain }}"
sig_gen_domain: "sig-gen.{{ domain }}"
eestecnet_fe_domain: "{{ domain }}"
eestecnet_be_domain: "api.{{ domain }}"
pgadmin_domain: "db.{{ domain }}"
wiki_domain: "wiki.{{ domain }}"
mattermost_domain: "chat.{{ domain }}"
preview_domain: "*.preview.{{ domain }}"

# Select which DKIM key to use
dkim_selector: development

# As of now, everything runs on the same host. If that changes, you need to
# move that to host_vars or group_vars.
certificate_domains:
  - "{{ domain }}"
  - "{{ eestecnet_be_domain }}"
  - "{{ pgadmin_domain }}"
  - "{{ sig_gen_domain }}"
  - "{{ ec_domain }}"
  - "{{ ec_second_domain }}"
  - "{{ ssa_domain }}"
  - "{{ ssa_wp_domain }}"
  - "{{ mattermost_domain }}"
  - "{{ wiki_domain }}"
  - "{{ preview_domain }}"

# Users and their options
users:
  - name: alex
    groups:
      - sudo
      - docker
  - name: glarakis
    groups:
      - sudo
      - docker
  - name: lavdelas
    groups:
      - sudo
      - docker
  - name: friday
    groups:
      - sudo
      - docker
  - name: katea
    groups:
      - sudo
      - docker
  - name: myron
    groups:
      - sudo
      - docker
  - name: bogdan
    groups:
      - sudo
      - docker
  - name: gitlab
    groups:
      - sudo
      - docker
  - name: jbm
    groups:
      - sudo
      - docker

# Users that can directly login to root without sudo (names separated by '|')
root_ssh_keys: "myron|alex|lavdelas|gitlab|jbm"

# Internal ports
# We use high port numbers for this because low port numbers require special
# privileges. You should never have to change that, if you need to publish
# them on other ports on the host, modify the 'publish' argument of the
# docker command. Make sure the application is properly secured before!
fastcgi_port: 9000
eestecnet_port: 8080
pgadmin_http_port: 80
wiki_http_port: 3000
mattermost_http_port: 8065
