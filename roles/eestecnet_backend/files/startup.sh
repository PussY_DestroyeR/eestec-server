#!/bin/bash

# Install (any new) packages.
composer install --no-dev

# Cache files to speed up the application
# Clear all application cache.
php artisan cache:clear
# Clean and cahce configuration files.
php artisan config:clear
php artisan config:cache
# Clean and cache event files (event listeners).
php artisan event:clear
php artisan event:cache
# Clean and cache routes.
php artisan route:clear
php artisan route:cache

# softlink from /docker/new_eestecnet_BE/data/ to ./public/storage
php artisan storage:link

# Run any new migrations, if they exist
php artisan migrate --force

# Make sure we don't start with empty stats
php artisan eestecnet:update-statistics

# Start the service
php-fpm
